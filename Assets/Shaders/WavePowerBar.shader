﻿Shader "Unlit/WavePowerBar"
{
	Properties
	{
		[PerRendererData] _MainTex ( "Texture", 2D ) = "white" {}
		_VisiblePercent ( "Visible Percentage", Float ) = 0.5
	}
	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector"="True"
			"RenderType" = "Transparent"
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Fog { Mode Off }
		Blend One OneMinusSrcAlpha

		Pass
		{
			Lighting Off

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;    // 0,0 => 1,1, bottom left is 0,0
			};

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color;

				return OUT;
			}

			sampler2D _MainTex;
			float _VisiblePercent;

			fixed4 frag(v2f IN) : SV_Target
			{
				// Get Texture
				fixed4 c = fixed4( 0, 0, 0, 0 );    // Default to NOTHING

				// Only draw things that are to the left of the Visible
				// percentage
				if (IN.texcoord.x < _VisiblePercent)
				{
					c = tex2D(_MainTex, IN.texcoord) * IN.color;
					c.rgb *= c.a;
				}

				return c;
			}

			ENDCG
		}
	}
}
