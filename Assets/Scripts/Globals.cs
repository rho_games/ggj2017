﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Globals
{
	public static int Score = 0;
	public static int GroupNum = 0;
	public static int Strikes = 0;
	public const int MaxStrikes = 5;
	public const int WaveMax = 11;
	public const int GroupMin = 6;
	public const int GroupMax = 18;
	public const int SpawnMin = 1;
	public const int SpawnMax = 3;
	public static int[] SpawnDelta =
	{
		5,
		5,4,
		4,3,
		4,3,2,
		3,2,1
	};
}
