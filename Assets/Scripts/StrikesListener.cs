﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Listens for if the player has exceeded the max num of strikes, and if so,
// throws an event
public class StrikesListener : MonoBehaviour
{
	// only throw event once
	bool eventThrown = false;

	// Update is called once per frame
	void Update ()
	{
		if (!eventThrown && Globals.Strikes >= Globals.MaxStrikes)
		{
			EventBroadcaster.broadcastEvent(new PlayerFired());
			eventThrown = true;
		}
	}
}
