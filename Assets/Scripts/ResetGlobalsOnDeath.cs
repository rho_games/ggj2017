﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetGlobalsOnDeath : MonoBehaviour
{
	// Use this for initialization
	void OnDisable ()
	{
		Globals.Score = 0;
		Globals.GroupNum = 0;
		Globals.Strikes = 0;
	}
}
