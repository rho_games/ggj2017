﻿using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;

public class WalkToDoor : MonoBehaviour {

	private GameObject placeToWalkTo;
	private Transform currentDirection;
	private Rigidbody rigidBody;
	private float speed = 5.0f;
	private ParticleSystem customerMood;
	public int currentRank;
    public bool inTheZone;
    public int spawnPoint;
    public int customerNumber;
    public WaveLogic waveLogic;
    public bool scored;

	[SerializeField] private Material angryMaterial;
	[SerializeField] private Material neutralMaterial;
	[SerializeField] private Material happyMaterial;

    // Use this for initialization
    void Start () 
	{
        waveLogic = FindObjectOfType<WaveLogic>();
		List<string> skinSet = new List<string> ();
		skinSet.Add ("0-white");
		skinSet.Add ("1-magenta");
		skinSet.Add ("2-red");
		skinSet.Add ("3-green");
		skinSet.Add ("4-blue");
		skinSet.Add ("5-yellow");
		Random.InitState ((int)System.DateTime.Now.Ticks);
		int random = Random.Range (0, 100);
		currentRank = random % 6;
		placeToWalkTo = GameObject.FindGameObjectsWithTag ("Exit")[random % 2];
		currentDirection = GetComponent<Transform> ();
        currentDirection.transform.position = GameObject.FindGameObjectsWithTag("Enter")[spawnPoint].transform.position;
        rigidBody = GetComponent<Rigidbody> ();
        var skel = GetComponent<SkeletonAnimation>();
        skel.Skeleton.SetSkin(skinSet[currentRank]);
        skel.state.SetAnimation(0, currentRank == 0 ? "shufflin" : "walkin", true);
        skel.state.Update(Random.Range(0.0f, 100.0f));
        inTheZone = false;
        scored = false;
		customerMood = GetComponent<ParticleSystem> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		currentDirection.position = Vector3.MoveTowards (currentDirection.position, placeToWalkTo.transform.position, speed * Time.deltaTime);
	}

	void OnDisable()
	{
        waveLogic.customer.Remove(transform.root.gameObject);
    }

	void OnTriggerEnter(Collider collision)
	{
		if (collision.gameObject.tag == "Exit") 
		{
			Destroy (gameObject);
		}
        else if(collision.gameObject.tag == "WaveZone")
        {
            inTheZone = true;
			gameObject.transform.GetChild (0).GetComponent<ParticleSystem> ().Play ();
        }
	}

	void OnTriggerExit(Collider collider)
	{
		if (collider.gameObject.tag == "WaveZone") 
		{
			speed = 15.0f;
			inTheZone = false;
			if (!scored) 
			{
				var scoreMood = 2 - currentRank;
				var tmpMood = scoreMood > 0 ? happyMaterial : 
					scoreMood == 0 ? neutralMaterial : angryMaterial;

				GetComponent<ParticleSystemRenderer> ().material = tmpMood;
				customerMood.Emit (1);
				gameObject.transform.GetChild (0).GetComponent<ParticleSystem> ().Play ();
			}
			if (!scored)
			{
				if (2 < currentRank)
				{
					Globals.Strikes += 1;
					Globals.Score += -1;
				}
				else
				{
					Globals.Score += (2 - currentRank);
				}
			}
		}
	}


    public void GenerateScore(int strength) {
        if(scored)
        {
            return;
        }

		speed = 15.0f;
        scored = true;
        int score = 2 - Mathf.Abs(currentRank - strength);

        if (0 > score)
        {
            Globals.Strikes += 1;
        }

        Globals.Score += (score < -1) ? -1 : score;

		var tmpMood = score > 0 ? happyMaterial : 
			score == 0 ? neutralMaterial : angryMaterial;

		GetComponent<ParticleSystemRenderer> ().material = tmpMood;
		customerMood.Emit (1);
		gameObject.transform.GetChild (0).GetComponent<ParticleSystem> ().Play ();
		BroadcastMessage( "OnScoreGenerated", score);
    }
}
