﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;

[RequireComponent( typeof(SkeletonAnimation), typeof(AudioSource))]
public class NPCFootsteps : MonoBehaviour
{
	[SerializeField] private AudioClip[] clips;

	private SkeletonAnimation spineAnim;
	private AudioSource source;

	// Use this for initialization
	void Start()
	{
		spineAnim = GetComponent<SkeletonAnimation>();
		source = GetComponent<AudioSource>();

		spineAnim.state.Event += AnimationEvent;
	}

	private int prevIndex = -1;

	void PlayRandom()
	{
		if (clips.Length == 0) return;

		int index = Random.Range(0, clips.Length);
		if (prevIndex == index)
		{
			index = (index + 1) % clips.Length;
		}

		prevIndex = index;

		source.clip = clips[index];
		source.Play();
	}

	public void AnimationEvent(TrackEntry trackEntry, Spine.Event e)
	{
		// If there is a step
		if (e.Data.Name == "step")
		{
			PlayRandom();
		}
	}
}
