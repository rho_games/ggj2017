﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// When player gets a score, play a random noise
[RequireComponent(typeof(AudioSource))]
public class NPCScoreSound : MonoBehaviour
{
	[SerializeField] private AudioClip[] malePositive;
	[SerializeField] private AudioClip[] maleNegative;
	[SerializeField] private AudioClip[] femalePositive;
	[SerializeField] private AudioClip[] femaleNegative;

	private AudioSource source;
	private WalkToDoor info;
	private const float delay = 0.3f;

	void Start ()
	{
		// Find NPC Info from parent
		info = transform.parent.GetComponent<WalkToDoor>();

		source = GetComponent<AudioSource>();
	}

	public void OnScoreGenerated(int score)
	{
		// Play positive
		if (score >= 0)
		{
			if (info.currentRank == 1 || info.currentRank == 2)
			{
				PlayRandomVoice( femalePositive );
			}
			else
			{
				PlayRandomVoice( malePositive );
			}
		}
		// play negative
		else
		{
			if (info.currentRank == 1 || info.currentRank == 2)
			{
				PlayRandomVoice( femaleNegative );
			}
			else
			{
				PlayRandomVoice( maleNegative );
			}
		}
	}

	// make sure we don't use the same voice twice in a row
	private int prevIndex = -1;

	void PlayRandomVoice(AudioClip[] clips)
	{
		if (clips.Length == 0) return;

		int index = Random.Range(0, clips.Length);
		if (prevIndex == index)
		{
			index = (index + 1) % clips.Length;
		}

		prevIndex = index;

		source.clip = clips[index];
		source.PlayDelayed(delay);
	}
}
