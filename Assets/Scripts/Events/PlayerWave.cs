﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;

class PlayerWave: IGameEvent
{
    public int strength;

    public PlayerWave(int str)
    {
        strength = str;
    }
}
