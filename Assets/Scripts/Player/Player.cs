﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	// Time it takes to reach maximum charge
	private const float MaxChargeTime = 2.0f;
	private float startChargeTime;

	// percentage point in which we should restart back to zero
	private const float RollOverPercetnage = 1.4f;

	float ChargePercentage
	{
		get
		{
			return (Time.time - startChargeTime) / MaxChargeTime;
		}
	}

	// Update is called once per frame
	void Update ()
	{
		// All of these input states could happen in the same frame and
		// therefore all need need to be triggered if that were the case
		if (Input.GetKeyDown(KeyCode.Space))
		{
			// Start charging
			startChargeTime = Time.time;
			BroadcastMessage("OnChargeBegin");
		}

		// fired every frame it's held down
		if (Input.GetKey(KeyCode.Space))
		{
			// If we want to roll over, then restart the charge
			if (ChargePercentage > RollOverPercetnage)
			{
				startChargeTime = Time.time;
			}

			EventBroadcaster.broadcastEvent(
				new PlayerCharging(ChargePercentage)
			);
		}

		if (Input.GetKeyUp(KeyCode.Space))
		{
			// scale 1 -> 5 (inclusive)
			int strength = Mathf.Min((int) (ChargePercentage * 5 + 1), 5);

			EventBroadcaster.broadcastEvent(new PlayerWave(strength));
			BroadcastMessage("OnWave", strength);
			EventBroadcaster.broadcastEvent(
				new PlayerCharging(0.0f)
			);
		}
	}
}
