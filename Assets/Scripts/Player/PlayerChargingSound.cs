﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerChargingSound : MonoBehaviour
{
	[SerializeField] private AudioClip clip;
	private AudioSource source;

	// Use this for initialization
	void Start ()
	{
		source = GetComponent<AudioSource>();
	}

	public void OnChargeBegin()
	{
		source.clip = clip;
		source.Play();
	}
}
