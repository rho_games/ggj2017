﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

// Just for handling player wave events
[RequireComponent( typeof(SkeletonAnimation))]
public class PlayerSpineHandler : MonoBehaviour
{
	private SkeletonAnimation spineAnim;

	[Header("Animation Names")]
	[SpineAnimation, SerializeField] public string Idle   = "Idle";
	[SpineAnimation, SerializeField] public string Charge = "Charge";
	[SpineAnimation, SerializeField] public string Wave_1 = "Wave_1";
	[SpineAnimation, SerializeField] public string Wave_2 = "Wave_2";
	[SpineAnimation, SerializeField] public string Wave_3 = "Wave_3";
	[SpineAnimation, SerializeField] public string Wave_4 = "Wave_4";
	[SpineAnimation, SerializeField] public string Wave_5 = "Wave_5";

	// Use this for initialization
	void Start ()
	{
		spineAnim = GetComponent<SkeletonAnimation>();

		// Set idle animation
		spineAnim.state.SetAnimation(0, Idle, true);
	}

	// 1 -> 5
	public void OnWave(int waveStrength)
	{
		// Based off wave strength, trigger animation
		switch (waveStrength)
		{
			case 1:
				spineAnim.state.SetAnimation(0, Wave_1, false);
				break;
			case 2:
				spineAnim.state.SetAnimation(0, Wave_2, false);
				break;
			case 3:
				spineAnim.state.SetAnimation(0, Wave_3, false);
				break;
			case 4:
				spineAnim.state.SetAnimation(0, Wave_4, false);
				break;
			case 5:
				spineAnim.state.SetAnimation(0, Wave_5, false);
				break;
		}

		// Return back to the idle animation
		spineAnim.state.AddAnimation(0, Idle, true, 0.0f);
	}

	public void OnChargeBegin()
	{
		spineAnim.state.SetAnimation(0, Charge, true);
	}
}
