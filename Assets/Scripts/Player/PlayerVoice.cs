﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlayerVoice : MonoBehaviour
{
	[SerializeField] private AudioClip[] wave1Clips;
	[SerializeField] private AudioClip[] wave2Clips;
	[SerializeField] private AudioClip[] wave3Clips;
	[SerializeField] private AudioClip[] wave4Clips;
	[SerializeField] private AudioClip[] wave5Clips;

	private AudioSource source;

	void Start()
	{
		source = GetComponent<AudioSource>();
	}

	// make sure we don't use the same voice twice in a row
	private int prevIndex = -1;

	void PlayRandomVoice(AudioClip[] clips)
	{
		if (clips.Length == 0) return;

		int index = Random.Range(0, clips.Length);
		if (prevIndex == index)
		{
			index = (index + 1) % clips.Length;
		}

		prevIndex = index;

		source.clip = clips[index];
		source.Play();
	}

	public void OnWave(int waveStrength)
	{
		// Play audio based off given strength
		switch (waveStrength)
		{
			case 1:
				PlayRandomVoice(wave1Clips);
				break;
			case 2:
				PlayRandomVoice(wave2Clips);
				break;
			case 3:
				PlayRandomVoice(wave3Clips);
				break;
			case 4:
				PlayRandomVoice(wave4Clips);
				break;
			case 5:
				PlayRandomVoice(wave5Clips);
				break;
		}

	}
}
