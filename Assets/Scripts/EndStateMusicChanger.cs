﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Change music on end game state
[RequireComponent(typeof(AudioSource))]
public class EndStateMusicChanger : MonoBehaviour, IEventHandler
{
	[SerializeField] private AudioClip survivedMusic, firedMusic;

	private AudioSource audioSource;

	// Use this for initialization
	void Start ()
	{
		audioSource = GetComponent<AudioSource>();

		EventBroadcaster.registerHandler<SurvivedGame>(this);
		EventBroadcaster.registerHandler<PlayerFired>(this);
	}

	void OnDisable()
	{
		EventBroadcaster.unregsterHandler(this);
	}

	public void handleEvent(IGameEvent evt)
	{
		if (evt is SurvivedGame)
		{
			audioSource.clip = survivedMusic;
		}
		else if (evt is PlayerFired)
		{
			audioSource.clip = firedMusic;
		}

		audioSource.Play();
	}
}
