﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateOnRoundEnd : MonoBehaviour, IEventHandler
{
	[SerializeField] GameObject[] objectsToActivate;
	[SerializeField] GameObject[] objectsToDeactivate;

	// Use this for initialization
	void Start ()
	{
		EventBroadcaster.registerHandler<SurvivedGame>(this);
	}

	void OnDisable()
	{
		EventBroadcaster.unregsterHandler(this);
	}

	public void handleEvent( IGameEvent evt )
	{
		foreach (var obj in objectsToActivate)
		{
			obj.SetActive(true);
		}

		foreach (var obj in objectsToDeactivate)
		{
			obj.SetActive(false);
		}
	}
}
