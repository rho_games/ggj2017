﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class StrikeText : MonoBehaviour
{
	Text strikeText;

	void Start()
	{
		strikeText = GetComponent<Text>();
	}

	// Update is called once per frame
	void Update ()
	{
		strikeText.text = string.Format(
			"{0} / {1}",
			Globals.Strikes,
			Globals.MaxStrikes
		);
	}
}
