﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraRotation : MonoBehaviour
{
	private Transform trans;
	float speed = -2;
	float timeToChange = 20;

	// Use this for initialization
	void Start ()
	{
		trans = GetComponent<Transform>();

		StartCoroutine(SwitchDirection());
	}

	// Every timeToChange seconds, change rotation
	IEnumerator SwitchDirection()
	{
		while (true)
		{
			yield return new WaitForSeconds(timeToChange);
			speed = -speed;
		}
	}

	// Update is called once per frame
	void Update ()
	{
		trans.RotateAround(
			Vector3.zero,
			Vector3.up,
			speed * Time.deltaTime
		);
	}
}
