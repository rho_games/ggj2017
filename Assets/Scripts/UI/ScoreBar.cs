﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBar : MonoBehaviour
{
	[SerializeField]
	Text scoreText;

	[SerializeField] Color goodColor, badColor;

	// Use this for initialization
	void Start ()
	{
	}

	// Update is called once per frame
	void Update ()
	{
		scoreText.text = string.Format("Score : {0}", Globals.Score);
		scoreText.color = Globals.Score < 0 ? badColor : goodColor;
	}
}
