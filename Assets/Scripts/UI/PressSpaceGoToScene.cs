﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PressSpaceGoToScene : MonoBehaviour 
{
	[SerializeField, Scene] private string sceneName;

	void Update()
	{
		if (Input.GetKeyUp("space"))
		{
			SceneManager.LoadScene(sceneName);
		}
	}
}
