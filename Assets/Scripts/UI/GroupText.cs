﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GroupText : MonoBehaviour 
{
	[SerializeField]
	Text waveText;

	// Update is called once per frame
	void Update ()
	{
		waveText.text = string.Format("Group: {0}", Globals.GroupNum);
	}
}
