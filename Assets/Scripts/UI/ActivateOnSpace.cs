﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateOnSpace : MonoBehaviour
{
	[SerializeField] GameObject[] objectsToActivate;
	[SerializeField] GameObject[] objectsToDeactivate;

	// Use this for initialization
	void Update()
	{
		if (Input.GetKeyUp(KeyCode.Space))
		{
			foreach (var obj in objectsToActivate)
			{
				obj.SetActive(true);
			}

			foreach (var obj in objectsToDeactivate)
			{
				obj.SetActive(false);
			}
		}
	}
}
